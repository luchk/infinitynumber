////
//// infNum.cpp
//// Created by Taras on 11/1/19.
//#ifndef _infNum_cpp_
//#define _infNum_cpp_

#include "infNum.h"
#include "rev.h"
using namespace std;


infinityNum infinityNum::operator+(infinityNum a){
	string num1 = this->num;
	string num2 = a.getInfNum();
	string num3 = "";
	string num3Temp = "";
	string temp1, temp2;
	int remainder = 0;
	int number = 0;
		
	if(num1.size() > num2.size()){
		int diff = num1.size() - num2.size();
		num2 = rev(num2);
		for (int i = 0;i < diff;i++) {
			num2+="0";
		}
		num2 = rev(num2);
	}
	
	if(num1.size() < num2.size()){
		int diff = num2.size() - num1.size();
		num1 = rev(num1);
		for (int i = 0;i < diff ;i++) {
			num1 += "0";
		}
		num1 = rev(num1);
	}	
	
	
	for(int i = num1.size() - 1; i >= 0; i--){
		temp1 = num1[i];
		temp2 = num2[i];
		number = stoi(temp1)+stoi(temp2) + remainder;
		if (number >= 10){
			remainder = 1;
			number -= 10;
			num3 += to_string(number);
			continue;
		} 
		remainder = 0;
		num3 += to_string(number);
	}
	
	if(remainder != 0)
		num3 += to_string(remainder);
	string num4(num3.rbegin(), num3.rend());
	return num4;
}

infinityNum infinityNum::operator+(int a){
	string num1 = this->num;
	string num2 = to_string(a);
	string num3 = "";
	string num3Temp = "";
	string temp1, temp2;
	int remainder = 0;
	int number = 0;
		
	if(num1.size() > num2.size()){
		int diff = num1.size() - num2.size();
		num2 = rev(num2);
		for (int i = 0;i < diff;i++) {
			num2+="0";
		}
		num2 = rev(num2);
	}
	
	if(num1.size() < num2.size()){
		int diff = num2.size() - num1.size();
		num1 = rev(num1);
		for (int i = 0;i < diff ;i++) {
			num1 += "0";
		}
		num1 = rev(num1);
	}	
	
	
	for(int i = num1.size() - 1; i >= 0; i--){
		temp1 = num1[i];
		temp2 = num2[i];
		number = stoi(temp1)+stoi(temp2) + remainder;
		if (number >= 10){
			remainder = 1;
			number -= 10;
			num3 += to_string(number);
			continue;
		} 
		remainder = 0;
		num3 += to_string(number);
	}
	
	if(remainder != 0)
		num3 += to_string(remainder);
	string num4(num3.rbegin(), num3.rend());
	return num4;
}
infinityNum infinityNum::operator+(string a){
	string num1 = this->num;
	string num2 = a;
	string num3 = "";
	string num3Temp = "";
	string temp1, temp2;
	int remainder = 0;
	int number = 0;
		
	if(num1.size() > num2.size()){
		int diff = num1.size() - num2.size();
		num2 = rev(num2);
		for (int i = 0;i < diff;i++) {
			num2+="0";
		}
		num2 = rev(num2);
	}
	
	if(num1.size() < num2.size()){
		int diff = num2.size() - num1.size();
		num1 = rev(num1);
		for (int i = 0;i < diff ;i++) {
			num1 += "0";
		}
		num1 = rev(num1);
	}	
	
	
	for(int i = num1.size() - 1; i >= 0; i--){
		temp1 = num1[i];
		temp2 = num2[i];
		number = stoi(temp1)+stoi(temp2) + remainder;
		if (number >= 10){
			remainder = 1;
			number -= 10;
			num3 += to_string(number);
			continue;
		} 
		remainder = 0;
		num3 += to_string(number);
	}
	
	if(remainder != 0)
		num3 += to_string(remainder);
	string num4(num3.rbegin(), num3.rend());
	return num4;
}



infinityNum& infinityNum::operator += (infinityNum a){
	*this = *this + a;
	return *this;
}

infinityNum& infinityNum::operator += (string a){
	*this = *this + a;
	return *this;
}




infinityNum infinityNum::operator-(infinityNum a){
		string num1 = this->num;
		string num2 = a.getInfNum();
		string num3 = "";
		string num3Temp = "";
		string temp1, temp2;
		int remainder = 0;
		int number = 0;
			
		if(num1.size() > num2.size()){
			int diff = num1.size() - num2.size();
			num2 = rev(num2);
			for (int i = 0;i < diff;i++) {
				num2+="0";
			}
			num2 = rev(num2);
		}
		
		if(num1.size() < num2.size()){
			int diff = num2.size() - num1.size();
			num1 = rev(num1);
			for (int i = 0;i < diff ;i++) {
				num1 += "0";
			}
			num1 = rev(num1);
		}	
		
		for(int i = num1.size() - 1; i >= 0; i--){
			temp1 = num1[i];
			temp2 = num2[i];
			number = stoi(temp1)-stoi(temp2) - remainder;
			if(number < 0){
				number += 10;
				remainder = 1;
				num3 += to_string(number);
				continue;
			}
			remainder = 0;
			num3 += to_string(number);

		}
		string num4(num3.rbegin(), num3.rend());
		return num4;
}

infinityNum infinityNum::operator-(string a){
		string num1 = this->num;
		string num2 = a;
		string num3 = "";
		string num3Temp = "";
		string temp1, temp2;
		int remainder = 0;
		int number = 0;
			
		if(num1.size() > num2.size()){
			int diff = num1.size() - num2.size();
			num2 = rev(num2);
			for (int i = 0;i < diff;i++) {
				num2+="0";
			}
			num2 = rev(num2);
		}
		
		if(num1.size() < num2.size()){
			int diff = num2.size() - num1.size();
			num1 = rev(num1);
			for (int i = 0;i < diff ;i++) {
				num1 += "0";
			}
			num1 = rev(num1);
		}	
		
		for(int i = num1.size() - 1; i >= 0; i--){
			temp1 = num1[i];
			temp2 = num2[i];
			number = stoi(temp1)-stoi(temp2) - remainder;
			if(number < 0){
				number += 10;
				remainder = 1;
				num3 += to_string(number);
				continue;
			}
			remainder = 0;
			num3 += to_string(number);

		}
		string num4(num3.rbegin(), num3.rend());
		return num4;
}

infinityNum& infinityNum::operator -= (infinityNum a){
	*this = *this - a;
	return *this;
}

infinityNum& infinityNum::operator -= (string a){
	*this = *this - a;
	return *this;
}


//infinityNum operator*(infinityNum a){
//	for(int)
//}
//infinityNum operator*(string a);





bool infinityNum::operator==(infinityNum a){
	return this->num == a.getInfNum() ? 1:0;
}

bool infinityNum::operator!=(infinityNum a){
	return this->num == a.getInfNum() ? 0:1;
}

bool infinityNum::operator<(infinityNum a){
	if(this->num.size() > a.getInfNum().size()){
		return 0;	
	}else if (this->num.size()  < a.getInfNum().size()){
		return 1;
	}
	
	if(this->num.size() == a.getInfNum().size()){
		for(int i = 0; i < a.getInfNum().size(); i++){
			if(this->num[i] == a.getInfNum()[i]){
				continue;
			}
			string temp1 = "";
			temp1 += this->num[i];
			string temp2 = "";
			temp2 += a.getInfNum()[i];
			
			return (stoi(temp1) > stoi(temp2)) ?  0 :  1;
		}
	}
	
}
bool infinityNum::operator>(infinityNum a){
	return !(this->num < a.getInfNum());
}
bool infinityNum::operator<=(infinityNum a){
	return (this->num > a.getInfNum() || this->num == a.getInfNum());
}
bool infinityNum::operator>=(infinityNum a){
		return (!(this->num > a.getInfNum()) || this->num == a.getInfNum());
}

infinityNum infinityNum::operator++(){
	infinityNum temp = 0;
	*this += 1;
	temp = this->num;
	return temp;
}
infinityNum infinityNum::operator--(){
	infinityNum temp = 0;
	*this -= 1;
	temp = this->num;
	return temp;
}
infinityNum infinityNum::operator++(int){
	infinityNum temp = *this;
	*this += 1;
	
	return temp;
}
infinityNum infinityNum::operator--(int){
	infinityNum temp = *this;
	*this -= 1;
	return temp;
}


//Check operator ++ (int)
//{
//	Check temp;
//	temp.i = i++;
//	return temp;
//}

//		Check operator ++()
//		{
//		   Check temp;
//		   ++i;
//		   temp.i = i;
//		   return temp;
//		}


//#endif