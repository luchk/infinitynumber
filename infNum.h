//
// infNum.h
// Created by Taras on 11/1/19.

#ifndef _infNum_h_
#define _infNum_h_

#include <iostream>
#include <string>
#include <cstring> 


using namespace std;

class infinityNum{
	string num;
	public:	
		infinityNum(){
			this->num = "0";
		}
		infinityNum(int a){
			this->num = to_string(a);
		}
		infinityNum(string a){
			this->num = a;
		}
		string getInfNum(){
			return this->num;
		}
		infinityNum(infinityNum &a){
			this->num = a.getInfNum();
		}
		
		infinityNum operator=(infinityNum a){
			this->num = a.getInfNum();
			return this->num;
		}
		infinityNum operator=(int a){
			this->num = to_string(a);
			return this->num;
		}
		infinityNum operator=(long int a){
			this->num = to_string(a);
			return this->num;
		}
		infinityNum operator=(long long int a){
			this->num = to_string(a);
			return this->num;
		}
		infinityNum operator=(unsigned int a){
			this->num = to_string(a);
			return this->num;
		}
		infinityNum operator=(unsigned long int a){
			this->num = to_string(a);
			return this->num;
		}
		infinityNum operator=(unsigned long long int a){
			this->num = to_string(a);
			return this->num;
		}
		
		infinityNum operator=(string a){
			this->num = a;
			return this->num;
		}

		/*-----addition------*/
		infinityNum operator+(infinityNum a);
		infinityNum operator+(int a);
		infinityNum operator+(string a);
		
		infinityNum operator+(long int a){
			return *this + to_string(a);	
		}
		infinityNum operator+(long long int a){
			return *this + to_string(a);	
		}
		infinityNum operator+(unsigned int a){
			return *this + to_string(a);	
		}
		infinityNum operator+(unsigned long int a){
			return *this + to_string(a);	
		}
		infinityNum operator+(unsigned long long int a){
			return *this + to_string(a);	
		}
		infinityNum& operator += (infinityNum a);
		infinityNum& operator += (string a);
		
		/*-----subtraction------*/
		infinityNum operator-(infinityNum a);
		infinityNum operator-(string a);
		
		infinityNum operator-(int a){
			return *this - to_string(a);	
		}
		infinityNum operator-(long int a){
			return *this - to_string(a);	
		}
		infinityNum operator-(long long int a){
			return *this - to_string(a);	
		}
		infinityNum operator-(unsigned int a){
			return *this - to_string(a);	
		}
		infinityNum operator-(unsigned long int a){
			return *this - to_string(a);	
		}
		infinityNum operator-(unsigned long long int a){
			return *this - to_string(a);	
		}
		infinityNum& operator -= (infinityNum a);
		infinityNum& operator -= (string a);
		
		/*-------multiplication--------*/
		infinityNum operator*(infinityNum a);
		infinityNum operator*(string a);
		
		/*---------comperison----------*/
		bool operator==(infinityNum a);
		bool operator!=(infinityNum a);
		bool operator<(infinityNum a);
		bool operator>(infinityNum a);
		bool operator<=(infinityNum a);
		bool operator>=(infinityNum a);
		
		/*---------increment dicrement---------*/
//		infinityNum operator++(int a);
//		infinityNum operator--(int a);
		
		infinityNum operator++();
		infinityNum operator--();
		infinityNum operator++(int);
		infinityNum operator--(int);
		
  		friend	ostream &operator<<( ostream &output, const infinityNum &D ) { 
				output << D.num;
				return output;            
		}
		 friend istream &operator>>( istream  &input, infinityNum &D ) { 
				input >> D.num;
				return input;            
		}
	
};

//#include "infNum.cpp"

#endif